#include "multiplication.h"

#include <stdexcept>

int64_t Multiply(int a, int b) {
    return (int64_t) (b) * a;
}
